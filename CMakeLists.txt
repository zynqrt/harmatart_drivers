cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)

project("Xenomai kernel modules" VERSION 0.1.0 LANGUAGES C)
set(CMAKE_C_STANDARD 90)
set(CMAKE_C_STANDARD_REQUIRED ON)

set(CMAKE_SYSTEM_PROCESSOR arm)
set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
find_package(KernelHeaders REQUIRED)

message(INFO "Kernel headers= ${KERNELHEADERS_INCLUDE_DIRS}")
add_definitions(-D__KERNEL__ -DMODULE)

add_executable(gpio_udd gpio_udd.c)
target_include_directories(gpio_udd PRIVATE ${KERNELHEADERS_INCLUDE_DIRS})

add_executable(stepper_udd stepper_udd.c)
target_include_directories(stepper_udd PRIVATE ${KERNELHEADERS_INCLUDE_DIRS})

add_executable(reg165_udd reg165_udd.c)
target_include_directories(reg165_udd PRIVATE ${KERNELHEADERS_INCLUDE_DIRS})

add_executable(reg595_udd reg595_udd.c)
target_include_directories(reg595_udd PRIVATE ${KERNELHEADERS_INCLUDE_DIRS})
