#obj-y += rtdm_test.o
#rtdm_test-y := rtdm_test.o
KDIR := /home/mesh/XilinxWorkspace/ZTurnDebian/sw/xenomai-zynq/linux-4.19.82-xenomai
PWD := $(shell pwd)


#obj-m += rtdm_cis.o
#obj-m += cis_driver.o
#obj-m += gpio_driver.o
obj-m += gpio_udd.o
obj-m += reg165_udd.o
obj-m += stepper_udd.o
obj-m += reg595_udd.o

all: 
	$(MAKE) -C $(KDIR) SUBDIRS=$(PWD) modules

clean:
	rm .*.cmd *.ko *.o *.mod.c
