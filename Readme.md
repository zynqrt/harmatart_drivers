# HarmateRT kernel modules

Модули ядра на основе Xenomai RTDM UDD.

Модуль ядра типа UUD мапят память на пространство пользователя и дают доступ к прерываниям.

Работа с памятью через __RT(mmap). Прерывания работают через sigwaitinfo/sigtimedwait и select, 
например [тут](https://xenomai.org/pipermail/xenomai/2018-March/038525.html).  

Описание devicetree строить на основе [примера](http://www.xillybus.com/tutorials/device-tree-zynq-3)


## rtdm_cis

Просто мапит память CIS контроллера на пространство пользователя. Адрес на шине AXI 0x43C20000.
