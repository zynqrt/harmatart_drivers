#include <linux/list.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_irq.h>
#include <rtdm/driver.h>
#include <rtdm/udd.h>


MODULE_DESCRIPTION("RTDM UDD driver for custom GPIO");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vladimir Meshkov");


#define RTDM_CLASS_TEST             222
#define RTTEST_PROFILE_VERSION      1

#define RTDM_SUBCLASS_GPIO_IRQ       4711
#define DEVICE_NAME                 "test"

struct gpio_udd {
	struct udd_device device;
	int index;
    void *regs;
	struct list_head next;
};

static LIST_HEAD(gpio_udd_devices);
static DEFINE_MUTEX(gpio_udd_lock);


static void write_reg(void *base, size_t offset, uint32_t val) {
    uint32_t *reg = (uint32_t *)((uint8_t *)base + offset);
    *reg = val;
}

static int gpio_udd_interrupt(struct udd_device *udd)
{
	/* ack device interrupt, UDD will call udd_notify_event() next */
    struct gpio_udd *gpio = container_of(udd, struct gpio_udd, device);;
    write_reg(gpio->regs, 0x0120, 0b11);
	return RTDM_IRQ_HANDLED;
}


static int gpio_udd_add(struct gpio_udd *gu, int subclass, int device_index, int irq, struct platform_device *pdev)
{
	int ret = -ENOMEM;

	gu->device.device_name = kasprintf(GFP_KERNEL, "gpio_udd%d", device_index);
	if (gu->device.device_name == NULL)
		return -ENOMEM;
	gu->device.device_flags = RTDM_NAMED_DEVICE | RTDM_EXCLUSIVE;
	gu->device.device_subclass = subclass;

	gu->device.mem_regions[0].name = "regs";
	gu->device.mem_regions[0].addr = pdev->resource->start;
	gu->device.mem_regions[0].len = resource_size(pdev->resource);
	gu->device.mem_regions[0].type = UDD_MEM_PHYS;
    gu->regs = ioremap(gu->device.mem_regions[0].addr, gu->device.mem_regions[0].len);
    rtdm_printk(KERN_INFO "mapmem: 0x%08X\n", (uint32_t)gu->regs);
    if (gu->regs < 0) {
        ret = -ENOMEM;
        goto fail_label;
    }
	gu->device.irq = irq;
	rtdm_printk(KERN_INFO "Set UDD interrupt number: %d\n", gu->device.irq);

	gu->device.ops.open = NULL;
	gu->device.ops.open = NULL;
	gu->device.ops.close = NULL;
	gu->device.ops.ioctl = NULL;
	gu->device.ops.mmap = NULL;
	gu->device.ops.interrupt = gpio_udd_interrupt;

	gu->index = device_index;
	ret = udd_register_device(&gu->device);
	if (ret) {
		rtdm_printk(KERN_ERR "udd_register_device ret %d\n", ret);
		goto fail_label;
	}
	return ret;

fail_label:
	kfree(gu->device.device_name);
	return ret;
}

static struct gpio_udd *gpio_udd_alloc(int subclass, int device_index, int irq, struct platform_device *pdev)
{
	struct gpio_udd *gu;
	size_t asize;
	int ret;

	asize = sizeof(*gu);
	gu = kzalloc(asize, GFP_KERNEL);
	if (gu == NULL)
		return ERR_PTR(-ENOMEM);

	ret = gpio_udd_add(gu, subclass, device_index, irq, pdev);
	if (ret) {
		rtdm_printk(KERN_ERR "gpio_udd_add FAILED\n");
		kfree(gu);
		return ERR_PTR(-EINVAL);
	}

	mutex_lock(&gpio_udd_lock);
	list_add(&gu->next, &gpio_udd_devices);
	mutex_unlock(&gpio_udd_lock);

	return gu;
}


static void gpio_udd_remove(struct gpio_udd *gu)
{
	mutex_lock(&gpio_udd_lock);
	list_del(&gu->next);
	mutex_unlock(&gpio_udd_lock);
	udd_unregister_device(&gu->device);
	kfree(gu->device.device_name);
}


int __init gpio_udd_init(void)
{
	struct device_node *np = NULL;
	struct platform_device *pdev;
	struct gpio_udd *gu;
	int irq;
	int device_index = 0;

	rtdm_printk("Init GPIO UDD driver\n");
	if (!rtdm_available()) {
		rtdm_printk("Non RT system\n");
		return -ENOSYS;
	}

	INIT_LIST_HEAD(&gpio_udd_devices);

	for (;;) {
		np = of_find_compatible_node(np, NULL, "mesh,gpio-1.00.a");
		if (np == NULL)
			break;

		rtdm_printk(KERN_INFO "Node GPIO found\n");
		pdev = of_find_device_by_node(np);
		irq = irq_of_parse_and_map(np, 0);
		of_node_put(np);

		if (pdev == NULL)
			break;
		
		rtdm_printk(KERN_INFO "reg:       0x%08X\n", pdev->resource->start);
    	rtdm_printk(KERN_INFO "reg size:  0x%08X\n", resource_size(pdev->resource));
    	rtdm_printk(KERN_INFO "Interrupt: %d\n", irq);

    	gu = gpio_udd_alloc(RTDM_SUBCLASS_GPIO_IRQ, device_index, irq, pdev);
    	if (IS_ERR(gu)) {
    		rtdm_printk(KERN_ERR "gpio_udd_alloc FAILED\n");
    		return -ENOMEM;
    	}

    	device_index++;
	}

	return 0;
}


void __exit gpio_udd_exit(void)
{
	struct gpio_udd *gu, *n;
	rtdm_printk(KERN_INFO "Exit GPIO UDD driver\n");
	
	mutex_lock(&gpio_udd_lock);
	list_for_each_entry_safe(gu, n, &gpio_udd_devices, next) {
		mutex_unlock(&gpio_udd_lock);
		gpio_udd_remove(gu);
		kfree(gu);
		mutex_lock(&gpio_udd_lock);
	}
	mutex_unlock(&gpio_udd_lock);
}


MODULE_LICENSE("GPL");
module_init(gpio_udd_init);
module_exit(gpio_udd_exit);
