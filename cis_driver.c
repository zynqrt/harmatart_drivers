#include <linux/list.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/err.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <rtdm/driver.h>

#define RTDM_CLASS_DEVICE       222
#define RTDM_SUBCLASS_CIS      4711


struct rtdm_cis {
	struct rtdm_driver driver;
	struct rtdm_device dev;
	struct list_head next;
	int index;
	resource_size_t address_start;
	resource_size_t address_size;
	rtdm_lock_t lock;
};


static LIST_HEAD(rtdm_ciss);
static DEFINE_MUTEX(cis_lock);


static int cis_open(struct rtdm_fd *fd, int oflags) 
{
	struct rtdm_device *dev = rtdm_fd_device(fd);
	struct rtdm_cis *cis;

	printk(KERN_INFO "CIS open\n");
	cis = container_of(dev, struct rtdm_cis, dev);
	printk(KERN_INFO "CIS open index: %d\n", cis->index);
	return 0;
}


static void cis_close(struct rtdm_fd *fd)
{
	struct rtdm_device *dev = rtdm_fd_device(fd);
	struct rtdm_cis *cis;

	printk(KERN_INFO "CIS close\n");
	cis = container_of(dev, struct rtdm_cis, dev);
	printk(KERN_INFO "CIS close index: %d\n", cis->index);
}



static int cis_mmap(struct rtdm_fd *fd, struct vm_area_struct *vma) 
{
	struct rtdm_device *dev = rtdm_fd_device(fd);
	struct rtdm_cis *cis;
	int ret;
	
	cis = container_of(dev, struct rtdm_cis, dev);
	printk(KERN_INFO "mmap on device index %d\n", cis->index);
	printk(KERN_INFO "device address: 0x%08X\n", cis->address_start);
	printk(KERN_INFO "device size: 0x%08X\n", cis->address_size);

	ret = rtdm_mmap_iomem(vma, cis->address_start);
	printk(KERN_INFO "iomem ret %d\n", ret);
	return ret;
}


static int rtdm_cis_add(struct rtdm_cis *rg, int cis_subclass, int index)
{
	int ret = -ENOMEM;

	rg->driver.profile_info = (struct rtdm_profile_info)
		RTDM_PROFILE_INFO(rtdm_cis, 
					RTDM_CLASS_DEVICE,
					cis_subclass,
					0);
	
	rg->driver.device_flags = RTDM_NAMED_DEVICE | RTDM_EXCLUSIVE;
	rg->driver.device_count = 1;
	rg->driver.context_size = 0;
	rg->driver.ops = (struct rtdm_fd_ops) {
		.open = cis_open,
		.close = cis_close,
		.write_rt = NULL,
		.read_rt = NULL,
		.ioctl_rt = NULL,
		.select = NULL,
		.mmap = cis_mmap,
	};
	rtdm_lock_init(&rg->lock);
	rg->dev.driver = &rg->driver;
	rg->dev.label = kasprintf(GFP_KERNEL, "cis%d", index);
	if (rg->dev.label == NULL) {
		printk(KERN_ERR "label alloc error\n");
		goto fail_label;
	}

	rg->index = index + 12;
	ret = rtdm_dev_register(&rg->dev);
	if (ret) {
		printk(KERN_ERR "rtdm_dev_register %d\n", ret);
		goto fail_label;
	}

	return ret;

fail_label:
	kfree(rg->dev.label);
	return ret;
}

static struct rtdm_cis *rtdm_cis_alloc(int type, int device_index)
{
	struct rtdm_cis *rg;
	size_t asize;
	int ret;

	asize = sizeof(*rg);
	rg = kzalloc(asize, GFP_KERNEL);
	if (rg == NULL)
		return ERR_PTR(-ENOMEM);

	ret = rtdm_cis_add(rg, type, device_index);
	if (ret) {
		printk(KERN_ERR "rtdm_cis_add FAILED\n");
		kfree(rg);
		return ERR_PTR(-EINVAL);
	}

	mutex_lock(&cis_lock);
	list_add(&rg->next, &rtdm_ciss);
	mutex_unlock(&cis_lock);

	return rg;
}


static int rtdm_cis_scan_of(struct device_node *from, const char *compat, int type)
{

	struct device_node *np = from;
	struct platform_device *pdev;
	struct rtdm_cis *rg;
	int device_index = 0;

	if (!rtdm_available())
		return -ENOSYS;

	INIT_LIST_HEAD(&rtdm_ciss);

	for (;;) {
		np = of_find_compatible_node(np, NULL, compat);
		if (np == NULL)
			break;

		printk(KERN_INFO "Node CIS found\n");
		pdev = of_find_device_by_node(np);
		of_node_put(np);
		if (pdev == NULL)
			break;

		printk(KERN_INFO "reg:      0x%08X\n", pdev->resource->start);
    	printk(KERN_INFO "reg size: 0x%08X\n", resource_size(pdev->resource));
    	rg = rtdm_cis_alloc(type, device_index);
    	if (IS_ERR(rg)) {
    		printk(KERN_INFO "rtdm_cis_alloc FAILED\n");
    	 	return -ENOMEM;
    	}

    	rg->address_start = pdev->resource->start;
    	rg->address_size = resource_size(pdev->resource);
    	device_index++;
	}
	return 0;
}


static void rtdm_cis_remove(struct rtdm_cis *rg)
{
	mutex_lock(&cis_lock);
	list_del(&rg->next);
	mutex_unlock(&cis_lock);

	rtdm_dev_unregister(&rg->dev);
	kfree(rg->dev.label);
}


static int __init cis_driver_init(void)
{
	printk("CIS Driver init\n");
	return rtdm_cis_scan_of(NULL, "xlnx,CISController-1.1", RTDM_SUBCLASS_CIS);
}
module_init(cis_driver_init);

static void __exit cis_driver_exit(void)
{
	struct rtdm_cis *rg, *n;
	printk("CIS Driver exit\n");
	mutex_lock(&cis_lock);

	list_for_each_entry_safe(rg, n, &rtdm_ciss, next) {
		mutex_unlock(&cis_lock);
		rtdm_cis_remove(rg);
		kfree(rg);
		mutex_lock(&cis_lock);
	}
	mutex_unlock(&cis_lock);
}
module_exit(cis_driver_exit);

MODULE_LICENSE("GPL");

