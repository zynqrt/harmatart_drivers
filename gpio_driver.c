#include <linux/list.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/err.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <rtdm/driver.h>

#define RTDM_CLASS_DEVICE       222
#define RTDM_SUBCLASS_GPIO      4710


struct rtdm_gpio {
    struct list_head next;
    
    rtdm_irq_t irqh;
    u32 event_count;
    struct rtdm_event pulse;
    
	struct rtdm_driver driver;
	struct rtdm_device dev;
	    
	int index;
	resource_size_t address_start;
	resource_size_t address_size;
	rtdm_lock_t lock;
    int irq;
    
};

struct rtdm_gpio_context
{
	int value;
};

static LIST_HEAD(rtdm_gpios);
static DEFINE_MUTEX(gpio_lock);



static int gpio_open(struct rtdm_fd *fd, int oflags) 
{
	struct rtdm_gpio_context *ctx = rtdm_fd_to_private(fd);
	struct rtdm_device *dev = rtdm_fd_device(fd);
	struct rtdm_gpio *gpio;

	printk(KERN_INFO "GPIO open\n");
	gpio = container_of(dev, struct rtdm_gpio, dev);
	printk(KERN_INFO "GPIO open index: %d\n", gpio->index);
	ctx->value = 0;

	return 0;
}


static void gpio_close(struct rtdm_fd *fd)
{
	// struct rtdm_gpio_context *ctx = rtdm_fd_to_private(fd);
	struct rtdm_device *dev = rtdm_fd_device(fd);
	struct rtdm_gpio *gpio;

	printk(KERN_INFO "GPIO close\n");
	gpio = container_of(dev, struct rtdm_gpio, dev);
	printk(KERN_INFO "GPIO close index: %d\n", gpio->index);
}


static ssize_t gpio_write_rt(struct rtdm_fd *fd,
				 const void __user *buf, size_t len)
{
	struct rtdm_gpio_context *ctx = rtdm_fd_to_private(fd);
	struct rtdm_device *dev = rtdm_fd_device(fd);
	struct rtdm_gpio *gpio;
	int value, ret;

	if (len < sizeof(value))
		return -EINVAL;
	ret = rtdm_safe_copy_from_user(fd, &value, buf, sizeof(value));
	if (ret)
		return ret;

	gpio = container_of(dev, struct rtdm_gpio, dev);
	printk(KERN_INFO "Write value %d to GPIO, index (%d)\n", value, gpio->index);
	ctx->value = value;
	return sizeof(value);
}


static ssize_t gpio_read_rt(struct rtdm_fd *fd,
				void __user *buf, size_t len)
{
	struct rtdm_gpio_context *ctx = rtdm_fd_to_private(fd);
	int value, ret;

	if (len < sizeof(value))
		return -EINVAL;

	value = ctx->value + 5;
	ret = rtdm_safe_copy_to_user(fd, buf, &value, len);
	return ret ?: len;
}


static int rtdm_gpio_mmap(struct rtdm_fd *fd, struct vm_area_struct *vma) 
{
	struct rtdm_device *dev = rtdm_fd_device(fd);
	struct rtdm_gpio *gpio;
	int ret;
	
	gpio = container_of(dev, struct rtdm_gpio, dev);
	printk(KERN_INFO "mmap on device index %d\n", gpio->index);
	printk(KERN_INFO "device address: 0x%08X\n", gpio->address_start);
	printk(KERN_INFO "device size: 0x%08X\n", gpio->address_size);

	ret = rtdm_mmap_iomem(vma, gpio->address_start);
	printk(KERN_INFO "iomem ret %d\n", ret);
	return ret;
}


static int rtdm_gpio_add(struct rtdm_gpio *rg, int gpio_subclass, int index)
{
	int ret = -ENOMEM;

	rg->driver.profile_info = (struct rtdm_profile_info)
		RTDM_PROFILE_INFO(rtdm_gpio, 
					RTDM_CLASS_DEVICE,
					gpio_subclass,
					0);
	
	rg->driver.device_flags = RTDM_NAMED_DEVICE | RTDM_EXCLUSIVE;
	rg->driver.device_count = 1;
	rg->driver.context_size = sizeof(struct rtdm_gpio_context);
	rg->driver.ops = (struct rtdm_fd_ops) {
		.open = gpio_open,
		.close = gpio_close,
		.write_rt = gpio_write_rt,
		.read_rt = gpio_read_rt,
		.ioctl_rt = NULL,
		.select = NULL,
		.mmap = rtdm_gpio_mmap,
	};
	rtdm_lock_init(&rg->lock);
	rg->dev.driver = &rg->driver;
	rg->dev.label = kasprintf(GFP_KERNEL, "gpiomx%d", index);
	if (rg->dev.label == NULL) {
		printk(KERN_ERR "label alloc error\n");
		goto fail_label;
	}

	rg->index = index + 12;
	ret = rtdm_dev_register(&rg->dev);
	if (ret) {
		printk(KERN_ERR "rtdm_dev_register %d\n", ret);
		goto fail_label;
	}

	return ret;

fail_label:
	kfree(rg->dev.label);
	return ret;
}

static struct rtdm_gpio *rtdm_gpio_alloc(int type, int device_index)
{
	struct rtdm_gpio *rg;
	size_t asize;
	int ret;

	asize = sizeof(*rg);
	rg = kzalloc(asize, GFP_KERNEL);
	if (rg == NULL)
		return ERR_PTR(-ENOMEM);

	ret = rtdm_gpio_add(rg, type, device_index);
	if (ret) {
		printk(KERN_ERR "rtdm_gpio_add FAILED\n");
		kfree(rg);
		return ERR_PTR(-EINVAL);
	}

	mutex_lock(&gpio_lock);
	list_add(&rg->next, &rtdm_gpios);
	mutex_unlock(&gpio_lock);

	return rg;
}


static int rtdm_gpio_scan_of(struct device_node *from, const char *compat, int type)
{

	struct device_node *np = from;
	struct platform_device *pdev;
	struct rtdm_gpio *rg;
	int device_index = 0;

	if (!rtdm_available())
		return -ENOSYS;

	INIT_LIST_HEAD(&rtdm_gpios);

	for (;;) {
		np = of_find_compatible_node(np, NULL, compat);
		if (np == NULL)
			break;

		printk(KERN_INFO "Node GPIO found\n");
		pdev = of_find_device_by_node(np);
		of_node_put(np);
		if (pdev == NULL)
			break;

		printk(KERN_INFO "reg:      0x%08X\n", pdev->resource->start);
    	printk(KERN_INFO "reg size: 0x%08X\n", resource_size(pdev->resource));
    	rg = rtdm_gpio_alloc(type, device_index);
    	if (IS_ERR(rg)) {
    		printk(KERN_INFO "rtdm_gpio_alloc FAILED\n");
    	 	return -ENOMEM;
    	}

    	rg->address_start = pdev->resource->start;
    	rg->address_size = resource_size(pdev->resource);
    	device_index++;
	}
	return 0;
}


static void rtdm_gpio_remove(struct rtdm_gpio *rg)
{
	mutex_lock(&gpio_lock);
	list_del(&rg->next);
	mutex_unlock(&gpio_lock);

	rtdm_dev_unregister(&rg->dev);
	kfree(rg->dev.label);
}


static int __init gpio_driver_init(void)
{
	printk("GPIO Driver init\n");
	return rtdm_gpio_scan_of(NULL, "mesh,gpio-1.00.a", RTDM_SUBCLASS_GPIO);
}
module_init(gpio_driver_init);

static void __exit gpio_driver_exit(void)
{
	struct rtdm_gpio *rg, *n;
	printk("GPIO Driver exit\n");
	mutex_lock(&gpio_lock);

	list_for_each_entry_safe(rg, n, &rtdm_gpios, next) {
		mutex_unlock(&gpio_lock);
		rtdm_gpio_remove(rg);
		kfree(rg);
		mutex_lock(&gpio_lock);
	}
	mutex_unlock(&gpio_lock);
}
module_exit(gpio_driver_exit);

MODULE_LICENSE("GPL");


