#include <linux/kernel.h>
#include <linux/module.h>
#include <rtdm/driver.h>
#include <linux/init.h>
#include <rtdm/udd.h>


#define RTDM_SUBCLASS_GPIO_IRQ      4711
#define DEVICE_NAME                 "cis"

MODULE_DESCRIPTION("RTDM driver for Test Open Read Write");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vladimir Mesh");

#define MAP_FAILED  ((void *) -1)


#define CIS_CONTROLLER_BASE 0x43C20000


static int cis_open(struct rtdm_fd *fd, int oflags)
{
    rtdm_printk("RTDM UDD open\n");
    return 0;
}

static void cis_close(struct rtdm_fd *fd)
{
    rtdm_printk("RTDM UDD close\n");
}

static int cis_mmap(struct rtdm_fd *fd, struct vm_area_struct *vma)
{
struct udd_memregion *rn;
struct udd_device *udd;
int ret;

    rtdm_printk("RTDM UDD mmap\n");
    udd = udd_get_device(fd);
    rn = udd->mem_regions + rtdm_fd_minor(fd);

    switch (rn->type) {
    case UDD_MEM_PHYS:
        ret = rtdm_mmap_iomem(vma, rn->addr);
        break;
    case UDD_MEM_LOGICAL:
        ret = rtdm_mmap_kmem(vma, (void *)rn->addr);
        break;
    case UDD_MEM_VIRTUAL:
        ret = rtdm_mmap_vmem(vma, (void *)rn->addr);
        break;
    default:
        ret = -EINVAL;
    }
    return ret;
}

int irq_handler(struct udd_device *udd)
{
    rtdm_printk("RTDM driver: Got IRQ () !\n");
    return RTDM_IRQ_HANDLED;
}

/*
static void *mapmem(const char *msg, size_t size, off_t off)
{
    void* map = ioremap(off, size);
    if (map < 0)
        printk(KERN_ERR "%s: mapping 0x%08lx for %s failed\n", __FUNCTION__, off, msg);
    else
        printk(KERN_DEBUG "%s: mapping 0x%08lx for %s succeded to 0x%08x\n", __FUNCTION__, off, msg, (uint32_t)map);
    return map;
}
*/


static struct udd_device device = {
    .device_name         = "test",
    .device_flags        = RTDM_NAMED_DEVICE | RTDM_EXCLUSIVE,
    .device_subclass     = RTDM_SUBCLASS_GPIO_IRQ,
    .mem_regions[0].name = "virt",
    .mem_regions[0].addr = CIS_CONTROLLER_BASE,
    .mem_regions[0].len  = 64*1024,
    .mem_regions[0].type = UDD_MEM_PHYS,
    .irq = UDD_IRQ_NONE,
    .ops = {
        .mmap = cis_mmap,
        .open = cis_open,
        .close = cis_close
    },
};


int __init cis_init(void)
{
    int ret;
    rtdm_printk("Test UDD, loading\n");

    /*
    cis_controller = mapmem("CIS", cis_controller_size, (uint32_t)cis_controller_base);
    if (cis_controller == MAP_FAILED) {
        printk(KERN_ERR "mapmem failed\n");
    } else {
        printk(KERN_DEBUG "cis remapped: 0x%08X\n", (uint32_t)cis_controller);
        cis_controller_reg_ip = cis_controller + 0x14/4;
    }
    */

    rtdm_printk("mem region len: %d\n", device.mem_regions[0].len);
    rtdm_printk("mem region addr: 0x%08lx\n", device.mem_regions[0].addr);

    ret = udd_register_device (&device);
    rtdm_printk("udd_register_device: %d\n", ret);
    return ret;
}


void __exit cis_exit(void)
{
    rtdm_printk("RPI_GPIO RTDM, unloading\n");
    udd_unregister_device (&device);
}


MODULE_LICENSE("GPL");
module_init(cis_init);
module_exit(cis_exit);
