#include <linux/list.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_irq.h>
#include <rtdm/driver.h>
#include <rtdm/udd.h>
#include "harmata_rtdm.h"


MODULE_DESCRIPTION("RTDM UDD driver for HC595 chain");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vladimir Meshkov");


struct reg595_udd {
    struct udd_device device;
    int index;
    void *regs;
    struct list_head next;
};

static LIST_HEAD(reg595_udd_devices);
static DEFINE_MUTEX(reg595_udd_lock);

static int reg595_udd_add(struct reg595_udd *ru, int subclass, int device_index, int irq, struct platform_device *pdev)
{
    int ret = -ENOMEM;

    ru->device.device_name = kasprintf(GFP_KERNEL, "reg595_udd%d", device_index);
    if (ru->device.device_name == NULL)
        return -ENOMEM;
    ru->device.device_flags = RTDM_NAMED_DEVICE | RTDM_EXCLUSIVE;
    ru->device.device_subclass = subclass;

    ru->device.mem_regions[0].name = "regs";
    ru->device.mem_regions[0].addr = pdev->resource->start;
    ru->device.mem_regions[0].len = resource_size(pdev->resource);
    ru->device.mem_regions[0].type = UDD_MEM_PHYS;
    ru->regs = ioremap(ru->device.mem_regions[0].addr, ru->device.mem_regions[0].len);
    rtdm_printk(KERN_INFO "mapmem: 0x%08X\n", (uint32_t)ru->regs);
    if (ru->regs < 0) {
        ret = -ENOMEM;
        rtdm_printk(KERN_ERR "ioremap error\n");
        goto fail_label;
    }
    ru->device.irq = irq;
    rtdm_printk(KERN_INFO "Set UDD interrupt number %d\n", ru->device.irq);

    ru->device.ops.open = NULL;
    ru->device.ops.close = NULL;
    ru->device.ops.ioctl = NULL;
    ru->device.ops.mmap = NULL;
    ru->device.ops.interrupt = NULL;
    ru->index = device_index;
    ret = udd_register_device(&ru->device);
    if (ret) {
        rtdm_printk(KERN_ERR "udd_register_device FAILED: %d\n", ret);
        goto fail_label;
    }
    return ret;

    fail_label:
    kfree(ru->device.device_name);
    return ret;
}

static struct reg595_udd *reg595_udd_alloc(int subclass, int device_index, int irq, struct platform_device *pdev)
{
    struct reg595_udd *ru;
    size_t asize;
    int ret;

    asize = sizeof(*ru);
    ru = kzalloc(asize, GFP_KERNEL);
    if (ru == NULL)
        return ERR_PTR(-ENOMEM);

    ret = reg595_udd_add(ru, subclass, device_index, irq, pdev);
    if (ret) {
        rtdm_printk(KERN_ERR "reg595_udd_add FAILED: %d\n", ret);
        kfree(ru);
        return ERR_PTR(-EINVAL);
    }

    mutex_lock(&reg595_udd_lock);
    list_add(&ru->next, &reg595_udd_devices);
    mutex_unlock(&reg595_udd_lock);
    return ru;
}


static void reg595_udd_remove(struct reg595_udd *ru)
{
    mutex_lock(&reg595_udd_lock);
    list_del(&ru->next);
    mutex_unlock(&reg595_udd_lock);
    udd_unregister_device(&ru->device);
    kfree(ru->device.device_name);
}


int __init reg595_udd_init(void)
{
    struct device_node *np = NULL;
    struct platform_device *pdev;
    struct reg595_udd *ru;
    int irq;
    int device_index = 0;

    rtdm_printk(KERN_INFO "Init reg595 UDD driver\n");
    if (!rtdm_available()) {
        rtdm_printk(KERN_ERR "Non RT system\n");
        return -ENOSYS;
    }

    INIT_LIST_HEAD(&reg595_udd_devices);

    for (;;) {
        np = of_find_compatible_node(np, NULL, RTDM_SUBCLASS_REG595_NODE);
        if (np == NULL)
            break;

        rtdm_printk(KERN_INFO "Node %s found\n", RTDM_SUBCLASS_REG595_NODE);
        pdev = of_find_device_by_node(np);
        irq = irq_of_parse_and_map(np, 0);
        of_node_put(np);

        if (pdev == NULL)
            break;

        rtdm_printk(KERN_INFO "reg:       0x%08X\n", pdev->resource->start);
        rtdm_printk(KERN_INFO "reg size:  0x%08X\n", resource_size(pdev->resource));
        rtdm_printk(KERN_INFO "Interrupt: %d\n", irq);
        ru = reg595_udd_alloc(RTDM_SUBCLASS_REG595_IRQ, device_index, irq, pdev);
        if (IS_ERR(ru)) {
            rtdm_printk(KERN_ERR "reg595_udd_alloc FAILED\n");
            return -ENOMEM;
        }
        device_index++;
    }
    return 0;
}

void __exit reg595_udd_exit(void)
{
    struct reg595_udd *ru, *n;
    rtdm_printk(KERN_INFO "Exit reg595 UDD driver\n");

    mutex_lock(&reg595_udd_lock);
    list_for_each_entry_safe(ru, n, &reg595_udd_devices, next) {
        mutex_unlock(&reg595_udd_lock);
        reg595_udd_remove(ru);
        kfree(ru);
        mutex_lock(&reg595_udd_lock);
    }
    mutex_unlock(&reg595_udd_lock);
}

module_init(reg595_udd_init);
module_exit(reg595_udd_exit);
